#pragma once

const char* const kMsgOk = "ok";
const char* const kMsgUnknownCommand = "unknown command";

const char* const kCmdStartScript = "startscript";
const char* const kCmdStop = "stop";
const char* const kCmdSpeed = "speed";
const char* const kCmdRotation = "rot";
const char* const kCmdStopVideo = "stopvideo";
const char* const kCmdStartVideo = "startvideo";
const char* const kCmdRecord = "record";
const char* const kCmdStopRecord = "stoprecord";
const char* const kCmdStartDebug = "startdebug";
const char* const kCmdStopDebug = "stopdebug";
